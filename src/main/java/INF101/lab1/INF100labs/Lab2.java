package INF101.lab1.INF100labs;

/**
 * Implement the methods findLongestWords, isLeapYear and isEvenPositiveInt.
 * These programming tasks was part of lab2 in INF100 fall 2022/2023. You can find them here: https://inf100h22.stromme.me/lab/2/
 */
public class Lab2 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        
    }

    public static void findLongestWords(String word1, String word2, String word3) {
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public static boolean isLeapYear(int year) {
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public static boolean isEvenPositiveInt(int num) {
        throw new UnsupportedOperationException("Not implemented yet.");
    }

}
