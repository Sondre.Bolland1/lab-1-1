package INF101.lab1.INF100labs;

/**
 * Implement the methods multiplesOfSevenUpTo, multiplicationTable and crossSum.
 * These programming tasks was part of lab3 in INF100 fall 2022/2023. You can find them here: https://inf100h22.stromme.me/lab/3/
 */
public class Lab3 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs

    }

    public static void multiplesOfSevenUpTo(int n) {
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public static void multiplicationTable(int n) {
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    public static int crossSum(int num) {
        throw new UnsupportedOperationException("Not implemented yet.");
    }

}